package com.log4j2.fix.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Demo {

	private static Logger logger = LogManager.getLogger(Demo.class);
	
	public static void main(String[] args) {
		
		logger.debug("debug message log4j-v2.17.0");
		logger.info("info message log4j-v2.17.0");
		logger.error("error message log4j-v2.17.0");
		
	}

}
