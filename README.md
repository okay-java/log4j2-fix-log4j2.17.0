# log4j2-fix-log4j2.17.0

Video link 
https://youtu.be/rRxQJMrjQlY

## Getting started

Log4j2 vulnerability fix update 2021 | log4j2 fix | log4j2 vulnerability | log4j2.17.0 | okay java

FIX/Mitigation/Java6
Log4j1.x (if using jdbcAppender )
switch to Log4j2.3.1 (Java 6) 

FIX/Mitigation/Java7
Apache Log4j 2.x to 2.16.0  ??
switch to Log4j2.12.3 (Java 7) 

FIX/Mitigation/Java8
Apache Log4j 2.x to 2.16.0  ??
switch to Log4j2.17.0 (Java 8) 

Download log2j2.17.0 jars
https://www.apache.org/dyn/closer.lua/logging/log4j/2.17.0/apache-log4j-2.17.0-bin.zip

References
https://logging.apache.org/log4j/2.x/security.html
https://nvd.nist.gov/vuln/detail/CVE-2021-44228
https://nvd.nist.gov/vuln/detail/CVE-2021-4104
https://nvd.nist.gov/vuln/detail/CVE-2021-45046
https://nvd.nist.gov/vuln/detail/CVE-2021-45105
https://www.cisecurity.org/log4j-zero-day-vulnerability-response/

CVE-2021-44228
Apache Log4j2 2.0-beta9 through 2.12.1 and 2.13.0 through 2.15.0 JNDI features used in configuration, log messages, and parameters do not protect against attacker controlled LDAP and other JNDI related endpoints. 
this functionality has been completely removed From version 2.16.0.
https://nvd.nist.gov/vuln/detail/CVE-2021-44228

CVE-2021-45046
It was found that the fix to address CVE-2021-44228 in Apache Log4j 2.15.0 was incomplete in certain non-default configurations. 
Log4j 2.16.0 (Java 8) and 2.12.2 (Java 7) fix this issue by removing support for message lookup patterns and disabling JNDI functionality by default.
https://nvd.nist.gov/vuln/detail/CVE-2021-45046

CVE-2021-4104
JMSAppender in Log4j 1.2 is vulnerable to deserialization of untrusted data when the attacker has write access to the Log4j configuration.
Note this issue only affects Log4j 1.2 when specifically configured to use JMSAppender.
Apache Log4j 1.2 reached end of life in August 2015. Users should upgrade to Log4j 2.
https://nvd.nist.gov/vuln/detail/CVE-2021-4104

CVE-2021-45105
Apache Log4j2 versions 2.0-alpha1 through 2.16.0 (excluding 2.12.3) did not protect from uncontrolled recursion from self-referential lookups.
This issue was fixed in Log4j 2.17.0 and 2.12.3.
https://nvd.nist.gov/vuln/detail/CVE-2021-45105

log4j-zero-day-vulnerability
To simplify things, the current list of vulnerabilities and recommended fixes is listed here:
CVE-2021-44228 (CVSS score: 10.0) - A remote code execution vulnerability affecting Log4j versions from 2.0-beta9 to 2.14.1 (Fixed in version 2.15.0)
CVE-2021-45046 (CVSS score: 9.0) - An information leak and remote code execution vulnerability affecting Log4j versions from 2.0-beta9 to 2.15.0, excluding 2.12.2 (Fixed in version 2.16.0)
CVE-2021-45105 (CVSS score: 7.5) - A denial-of-service vulnerability affecting Log4j versions from 2.0-beta9 to 2.16.0 (Fixed in version 2.17.0)
CVE-2021-4104 (CVSS score: 8.1) - An untrusted deserialization flaw affecting Log4j version 1.2 (No fix available; Upgrade to version 2.17.0)
We recommend following the advice of Apache, which recommends updating to 2.17.0 immediately.
https://www.cisecurity.org/log4j-zero-day-vulnerability-response/

please subscribe okay java :)

